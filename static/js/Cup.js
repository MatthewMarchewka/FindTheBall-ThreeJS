function _Cup(object, count, posY) {
    this.numer = count
    var object = object
    var container = new THREE.Object3D()
    object.userData.number = count
    object.userData.type = "cup"
    object.userData.ball = "false"
    object.userData.positionY = posY
    container.add(object);

    var that = this

    var light = new THREE.SpotLight(0xfff1e0, 0.3, 300, Math.PI);
    light.position.y = 160
    container.add(light)

    function logicSwap(direction){
        var old = main.cups.find(x => x.numer === (that.numer+direction))
        old.numer = that.numer
        physicSwap(old, direction)
    }

    function physicSwap(old, direction){
        swapAnimations.push({object: that.getCup(), direction: direction, initPosition: {x: that.getCup().position.x, z:that.getCup().position.z}, destinationPosition:{x: that.getCup().position.x, z: that.getCup().position.z+(direction*250)}, clock: $.now(), time: 500*Math.abs(direction)})
        swapAnimations.push({object: old.getCup(), direction: -1*direction, initPosition: {x: that.getCup().position.x, z: that.getCup().position.z+(direction*250)}, destinationPosition:{x: that.getCup().position.x, z:that.getCup().position.z}, clock: $.now(), time: 500*Math.abs(direction)})
    }

    this.moveLeft = function (force) {
        var type = "none"
        if (force < 200 && this.numer != 2) {
            type="light"
        } else if (this.numer == 0) {
            type="strong"
        } else if (this.numer == 1) {
            type="light"
        }

        if(type=="light"){
            //console.log("lightLeft")
            logicSwap(1)
            this.numer++
        }else if(type=="strong"){
            //console.log("strongLeft")
            logicSwap(2)
            this.numer+= 2
        }
    }

    this.moveRight = function (force) {
        var type = "none"
        if (force < 200 && this.numer != 0) {
            type="light"
        } else if (this.numer == 2) {
            type="strong"
        } else if (this.numer == 1) {
            type="light"
        }

        if(type=="light"){
            //console.log("lightRight")
            logicSwap(-1)
            this.numer--
        }else if(type=="strong"){
            //console.log("strongRight")
            logicSwap(-2)
            this.numer-= 2
        }

    }

    this.getModel = function () {
        return object
    }

    this.getCup = function () {
        return container
    }
}