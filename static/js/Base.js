function _Base(){

    var container = new THREE.Object3D()

    var geometry = new THREE.BoxGeometry(1000,10,1000)
    var blackBox = new THREE.MeshPhongMaterial({ 
        side: THREE.DoubleSide,
        shininess: 0,
        specular: 0xcccccc,
        map: new THREE.TextureLoader().load('gfx/woodBase.jpg') ,
        transparent: true, 
    })

    var base = new THREE.Mesh(geometry,blackBox)
    container.add(base)

    var light = new THREE.SpotLight(0xfff1e0, 0.4, 500, Math.PI);
    light.position.y = 160
    container.add(light)

    this.getBase = function(){
        return container
    }
}