var raycaster = new THREE.Raycaster();
var mouseVector = new THREE.Vector2()

$("#root").mousedown(function (e) {
    mouseVector.x = (e.clientX / $(window).width()) * 2 - 1;
    mouseVector.y = -(e.clientY / $(window).height()) * 2 + 1;
    raycaster.setFromCamera(mouseVector, camera);
    var intersects = raycaster.intersectObjects(scene.children, true);
    if (intersects.length > 0) {
        //start game
        if(intersects[0].object.userData.type == "cup" && player.startGame ){
            main.startGame(intersects[0].object)
            net.startGame(intersects[0].object.userData.number)
            player.startGame = false
            player.shuffling = true
            $("#prompt").text("PRESS ENTER TO SUBMIT")
        //shuffling cups
        }else if(intersects[0].object.userData.type == "cup" && player.shuffling){
            var counter = 0
            var cup = main.cups[intersects[0].object.userData.number]
            player.shuffleCount++
            if(player.shuffleCount == 16)
                main.endShuffling()
            setTimeout(function(){
                net.shuffling(intersects[0].object.userData.number,counter)
                main.shuffling(cup, counter)
            }, 300);

            $(document).on("mousemove", function(e){counter+=e.originalEvent.movementX})
        //choosing cup
        }else if(intersects[0].object.userData.type == "cup" && player.choosing){
            console.log(player.shuffleCount)
            if(intersects[0].object.userData.ball == "true"){
                $("#prompt").text("GOOD CHOICE")
                net.updateDB(player.shuffleCount*7)
            }else{
                $("#prompt").text("BAD CHOICE")
                net.updateDB(-1*(player.shuffleCount*7))
            }
            player.shuffleCount = 0
            player.choosing = false
            net.choosing(intersects[0].object.userData.number)
            liftingAnimations.push({object: intersects[0].object, initY: 0, clock: $.now(), time: 700, direction: 80, ball: intersects[0].object.userData.ball, source: "local"})
        }
        

    }
})

$(document).keydown(function(event){
    if(event.key == "Enter"){
        if($("#loginPanel").is(":visible")){
            net = new Net();
        }else{
            main.endShuffling()
        }
    }else if(event.key == "Escape"){
        if($("#loginPanel").is(":visible")){
            $("#login").val('')
        }else{
            net.endGame()
            main.endGame()
        }
    }else if(event.key == " "){
        if($("#endPanel").is(":visible"))
            location.reload();
    }else if(event.key == "i"){
        if(!($("#loginPanel").is(":visible"))&&!($("#rankingPanel").is(":visible"))){
            net.updateMoney()
            $("#infoPanel").toggle()
        }
    }else if(event.key == "r"){
        if(!($("#loginPanel").is(":visible"))&&!($("#infoPanel").is(":visible"))){
            net.getDB()
            $("#rankingPanel").toggle()
        }
    }else if(event.key == "t"){
        if(!($("#loginPanel").is(":visible"))){
            $("#tutorialPanel").toggle()
        }
    }
})

$(document).on("mouseup", function(){
    $(document).off("mousemove")
})