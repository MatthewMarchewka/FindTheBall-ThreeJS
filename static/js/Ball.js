function _Ball(){
    var container = new THREE.Object3D()

    var geometry = new THREE.SphereGeometry(20, 18, 8);
    var ballMaterial = new THREE.MeshBasicMaterial({ 
        side: THREE.DoubleSide,
        shininess: 0,
        specular: 0xcccccc,
        map: new THREE.TextureLoader().load('gfx/smoothBall.jpg') ,
        transparent: true, 
    })

    var base = new THREE.Mesh(geometry,ballMaterial)
    container.add(base)

    // var light = new THREE.SpotLight(0xfff1e0, 0.9, 150, Math.PI);
    // light.position.y = 30
    // container.add(light)

    this.getBall = function(){
        return container
    }
}