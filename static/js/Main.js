var scene, camera, renderer, axes

//tablice interakcji
var initAnimation = null
var swapAnimations = []
var liftingAnimations = []

function randomize(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function Main() {
    //renderer, kamera, scena
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(
        45, // kąt patrzenia kamery (FOV - field of view)
        $(window).width() / $(window).height(), // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
        0.1, // minimalna renderowana odległość
        10000 // maxymalna renderowana odległość
    );
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x808080);
    renderer.setSize($(window).width(), $(window).height())
    $("#root").append(renderer.domElement);
    //player 1 - 1200 300 500
    //player 2 - -200 300 500
    camera.position.set(1200, 300, 500)
    camera.lookAt(500, 10, 500)
    //************************* PODSTAWA *************************
    var base = new _Base()
    base.getBase().position.set(500, -1, 500)
    scene.add(base.getBase())
    //************************* KUBKI *************************
    this.cups = []
    // instantiate a loader
    var loader = new THREE.ObjectLoader();
    // load a resource
    loader.load(
        // resource URL
        'models/cup/cup_model.json',
        // called when resource is loaded
        function (object) {
            object.scale.set(8,8,8)
            object.rotation.y = Math.PI / 2;
            var box = new THREE.Box3().setFromObject( object );
            for(var i=0; i<Settings.cupsAmount; i++){
                var cup = new _Cup(object.clone(), i, (box.getSize().y/2)-4)
                cup.getCup().position.set(500,(box.getSize().y/2)-4,(i+1)*250)
                main.cups.push(cup)
                scene.add(cup.getCup());
            }   
        }
    );
    //************************* KULKA *************************
    var ball = new _Ball()
    ball.getBall().position.set(800, 22, 800)
    scene.add(ball.getBall())
    //************************* FUNKCJE PUBLICZNE *************************
    this.startGame = function(object){
        object.userData.ball = "true"
        initAnimation = {initPosition: {x: 800, z:800}, destinationPosition: object.parent.position, clock: $.now(), time: 1500, number: object.userData.number, positionY: object.userData.positionY}
    }

    this.shuffling = function(cup,counter){
        if(counter > 0){
            cup.moveRight(Math.abs(counter))
        }else if(counter < 0){
            cup.moveLeft(Math.abs(counter))
        }
    }

    this.endShuffling = function(){
        if(player.shuffling){
            $("#prompt").text("WAIT FOR ENEMY")
            player.shuffling = false
            net.endShuffling(player.shuffleCount)
            player.shuffleCount = 0
        }
    }

    this.endGame = function(){
        $("#endPanel").toggle()
    }
    //************************* RENDER *************************
    function render() {
        //start game animation
        if(initAnimation != null){
            if( ($.now()-initAnimation.clock) / initAnimation.time <= 1){
            //przesuwanie kulki
            ball.getBall().position.set(initAnimation.initPosition.x + (initAnimation.initPosition.x-initAnimation.destinationPosition.x)*(-1*Math.sin( (($.now()-initAnimation.clock) / initAnimation.time)*Math.PI/2)),
            22,
            initAnimation.initPosition.z + (initAnimation.initPosition.z-initAnimation.destinationPosition.z)*(-1*Math.sin( (($.now()-initAnimation.clock) / initAnimation.time)*Math.PI/2)))
            ball.getBall().rotation.x = (($.now()-initAnimation.clock) / initAnimation.time)*-10
            //podnoszenie kubka
            main.cups[initAnimation.number].getCup().position.y= initAnimation.positionY + 22* Math.sin( (($.now()-initAnimation.clock) / initAnimation.time)*Math.PI/2)
            main.cups[initAnimation.number].getCup().rotation.y= Math.sin( (($.now()-initAnimation.clock) / initAnimation.time)*Math.PI/2)*5* Math.PI/180
            main.cups[initAnimation.number].getCup().rotation.x= Math.sin( (($.now()-initAnimation.clock) / initAnimation.time)*Math.PI/2)*-30* Math.PI/180
            }else{
                main.cups[initAnimation.number].getCup().position.y= initAnimation.positionY
                main.cups[initAnimation.number].getCup().rotation.y = 0
                main.cups[initAnimation.number].getCup().rotation.x = 0
                THREE.SceneUtils.detach( ball.getBall(), main.cups[initAnimation.number].getCup(), scene );
                THREE.SceneUtils.attach( ball.getBall(), scene, main.cups[initAnimation.number].getCup() );
                ball.getBall().position.set(0,-50,0)
                initAnimation = null
            }
        }
        //przesuwanie kubka animacja
        if(swapAnimations.length != 0){
            for(var i=swapAnimations.length-1;i>=0; i--){
                if( ($.now()-swapAnimations[i].clock) / swapAnimations[i].time <= 1){
                    //przesuwanie kubka
                    swapAnimations[i].object.position.z = swapAnimations[i].initPosition.z + (swapAnimations[i].initPosition.z-swapAnimations[i].destinationPosition.z)*(-1*Math.sin( (($.now()-swapAnimations[i].clock) / swapAnimations[i].time)*Math.PI/2))
                    swapAnimations[i].object.position.x = swapAnimations[i].initPosition.x + (100*(-1*swapAnimations[i].direction*Math.sin( (($.now()-swapAnimations[i].clock) / swapAnimations[i].time)*Math.PI)))
                }else
                    swapAnimations.splice(i,1)
            }
        }
        if(liftingAnimations.length != 0){
            for(var i=liftingAnimations.length-1;i>=0; i--){
                if( ($.now()-liftingAnimations[i].clock) / liftingAnimations[i].time <= 1){
                    //przesuwanie kubka
                    liftingAnimations[i].object.position.y = liftingAnimations[i].initY + liftingAnimations[i].direction * Math.sin((($.now()-liftingAnimations[i].clock) / liftingAnimations[i].time)*Math.PI/2) 
                }else{
                    if(liftingAnimations[i].direction>0){
                        //opadanie kubka
                        liftingAnimations[i].initY = liftingAnimations[i].direction+10
                        liftingAnimations[i].direction*=-1
                        liftingAnimations[i].clock = $.now()
                        liftingAnimations.push(liftingAnimations[i])
                        if(liftingAnimations[i].ball == "false"){
                            if(liftingAnimations[i].source == "local" ){
                                player.startGame = true
                                $("#prompt").text("PRESS ENTER TO SUBMIT")
                            }
                            THREE.SceneUtils.detach( ball.getBall(), scene, liftingAnimations[i].object.parent );
                            THREE.SceneUtils.attach( ball.getBall(), liftingAnimations[i].object.parent, scene );
                            ball.getBall().position.set(800, 22, 800)
                        }else{
                            if(liftingAnimations[i].source == "local" ){
                                player.shuffling = true
                                $("#prompt").text("PRESS ENTER TO SUBMIT")
                            }
                        }
                    }
                    liftingAnimations.splice(i,1)
                }
            }
        }



        requestAnimationFrame(render);
        renderer.render(scene, camera);
    }

    render();
}