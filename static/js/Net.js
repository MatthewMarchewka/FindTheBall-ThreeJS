function Net() {
    var client = io();
    //pobranie armat z serwera
    client.on("onconnect", function (data) {
        if(data.waiting){
            player = new Player(0, "")
            $("#cupsImage").attr("src", "gfx/moving_cups.gif")
            $("#loginPanel").append("<br><b>Oczekiwanie na przeciwnika</>")
        }else{
            player = new Player(1, data.id)
            $("#loginPanel").toggle()
        }
        player.nick = $("#login").val()
        $("#nick").text("YOUR NICK IS "+$("#login").val())
        client.emit("addDB", {nick: player.nick, money: 100})
    })

    client.on("connectSecond", function (data) {
        $("#loginPanel").toggle()
        client.emit("addEnemy", {enemyID: data.id})
        player.enemySocketID = data.id 
    })

    client.on("endGame", function(data){
        main.endGame()
    })

    //start game function
    this.startGame = function(cupNumber){
        client.emit("startGame", {cupNumber: cupNumber, emitID: player.enemySocketID})
    }

    client.on("startGameInc", function (data) {
        //var cup = main.cups.find(x => x.numer === data.cup).getModel()
        var cup = main.cups[data.cup].getModel()
        main.startGame(cup)
    })
    //shuffle function
    this.shuffling = function(cupNumber, force){
        client.emit("shuffling", {cupNumber: cupNumber, force: force, emitID: player.enemySocketID})
    }

    client.on("shufflingInc", function (data) {
        var cup = main.cups[data.cup]
        main.shuffling(cup,data.force)
    })

    //end shuffling function
    this.endShuffling = function(count){
        console.log(count)
        client.emit("endShuffling", {emitID: player.enemySocketID, count: count})
    }

    client.on("endShufflingInc", function(data){
        $("#prompt").text("CHOOSE CUP WITH BALL")
        player.shuffleCount = data.count
        player.choosing = true 
    })
    //choosing function
    this.choosing = function(cupNumber){
        client.emit("choosing", {emitID: player.enemySocketID, cupNumber: cupNumber})
    }

    client.on("choosingInc", function(data){
        var cup = main.cups[data.cup].getModel()
        $("#prompt").text("WAIT FOR SHUFFLING SUBMIT")
        liftingAnimations.push({object: cup, initY: 0, clock: $.now(), time: 700, direction: 80, ball: cup.userData.ball, source: "online"})
    })

    this.endGame = function(){
        client.emit("endGame", {emitID: player.enemySocketID})
    }

    client.on("endGameInc", function(data){
        main.endGame()
    })

    this.updateDB = function(updateValue){
        client.emit("updateDB", {nick: player.nick, updateValue: updateValue})
        client.emit("updateEnemyDB", {emitID: player.enemySocketID, updateValue: -1*updateValue})
    }

    client.on("updateEnemyDBInc", function(data){
        client.emit("updateDB", {nick: player.nick, updateValue: data.updateValue})
    })
    
    this.updateMoney = function(){
        client.emit("updateMoney", {nick: player.nick})
    }

    client.on("updateMoneyInc", function(data){
        $("#score").text("YOUR MONEY IS: "+data.money)
    })

    this.getDB = function(){
        client.emit("getDB")
    }

    client.on("getDBInc", function(data){
        data.ranking.sort(function(a, b) { 
            return a.money - b.money
        });
        $("#ranking").text("")
        for(var j=0; j<10; j++){
            var i = data.ranking.length - (j+1)
            $("#ranking").append(data.ranking[i].nick+" "+data.ranking[i].money+"<br>")
        }
    })
}