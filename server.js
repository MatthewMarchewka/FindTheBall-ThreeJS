var http = require("http");
var fs = require("fs");
var qs = require("querystring");
var socketio = require("socket.io");
var mongoClient = require("mongodb").MongoClient;
var ObjectID = require('mongodb').ObjectID;

var database
var coll

var contentTypes = [
    {extension: "css", contentType: "text/css"},
    {extension: "js", contentType: "text/plain"},
    {extension: "jpg", contentType: "image/jpeg"},
    {extension: "png", contentType: "image/png"},
    {extension: "json", contentType: "application/json"},
    {extension: "gif", contentType: "image/gif"},
    {extension: "mp4", contentType: "video/mp4"}
]

var waitingId = null

var server = http.createServer(function(request,response){
    switch (request.method) {
        case "GET":
        var file = "static/index.html"
        var contentType = "text/html"
        var extensionIndex = request.url.indexOf(".") 
        for(var i=0; i<contentTypes.length; i++){
            if( request.url.slice(extensionIndex+1) == contentTypes[i].extension){
                contentType = contentTypes[i].contentType
                file = "static" + request.url
            }
        }
        if(contentType){
            fs.readFile(file, function (error, data) {
                response.writeHead(200, { 'Content-Type': contentType });
                response.write(data);
                response.end();
            })
        }
        break;
        case "POST":
            console.log('przyjmowanie danych ewentualne')
    } 
})

mongoClient.connect("mongodb://localhost/kubki", function (err, db) {
    if (err) console.log(err)
    else console.log("mongo podłączone")
    //tu można operować na utworzonej bazie danych db lub podstawić jej obiekt 
    // pod zmienną widoczną na zewnątrz
    database = db

    db.createCollection("users", function (err, coll) {
        coll = database.collection("users")
    })   
})

server.listen(3000, function(){
   console.log("serwer startuje na porcie 3000")
});

var io = socketio.listen(server) // server -> server nodejs

io.sockets.on("connection", function (client) {    
    //sprawdzanie oczekującego 
    var waiting
    var id

    if(waitingId){
        waiting = false
        id = waitingId   
        io.sockets.to(waitingId).emit("connectSecond", {id:client.id});
        client.enemyID = waitingId
        waitingId = null
    }else{
        waiting = true
        waitingId = client.id
    }
    
    client.emit("onconnect", {
        waiting: waiting,
        id: id
    })

    client.on("disconnect", function () {
        io.sockets.to(client.enemyID).emit("endGame", {serio: "yes"});
        if(waitingId == client.id)
            waitingId = null
    })

    client.on("addEnemy", function(data){
        client.enemyID = data.enemyID
    })

    client.on("startGame", function (data) {
        io.sockets.to(data.emitID).emit("startGameInc", {cup: data.cupNumber});
    })

    client.on("shuffling", function (data) {
        io.sockets.to(data.emitID).emit("shufflingInc", {cup: data.cupNumber, force: data.force});
    })

    client.on("endShuffling", function (data){
        io.sockets.to(data.emitID).emit("endShufflingInc", {count: data.count});
    })

    client.on("choosing", function(data){
        io.sockets.to(data.emitID).emit("choosingInc", {cup: data.cupNumber});       
    })

    client.on("endGame", function(data){
        io.sockets.to(data.emitID).emit("endGameInc", {prompt: "Your enemy left :("});       
    })

    client.on("addDB", function(data){
        database.collection("users").find({nick: data.nick}).toArray(function (err, items) {
            if(items.length == 0){
                database.collection("users").insert(data, function (err, result) {});
            }
        });
    })

    client.on("updateDB", function(data){
        database.collection("users").find({nick: data.nick}).toArray(function (err, items) {
            items[0].money += data.updateValue
            database.collection("users").update({nick: data.nick}, items[0] );
        })
    })
    
    client.on("updateEnemyDB", function(data){
        io.sockets.to(data.emitID).emit("updateEnemyDBInc", {updateValue: data.updateValue}); 
    })

    client.on("updateMoney", function(data){
        database.collection("users").find({nick: data.nick}).toArray(function (err, items) {
            io.sockets.to(client.id).emit("updateMoneyInc", {money: items[0].money});   
        })
    })

    client.on("getDB", function(){
        database.collection("users").find({}).toArray(function (err, items) {
            io.sockets.to(client.id).emit("getDBInc", {ranking: items});   
        })
    })

})

